#define FASTLED_ESP8266_RAW_PIN_ORDER
#include <FastLED.h>
#include <ArduinoOTA.h>
#include "image.h"

// Microcontroller specific defines
#define DATA_PIN 0
#define CLOCK_PIN 5
#define DELAY 4
#define NUM_LEDS2 128//32
// LED specific defines
#define COLOR_ORDER GRB
#define CHIPSET WS2812
#define BRIGHTNESS  128
void isr(void);
void setup() {
    // Add support for OTA***************************************
    ArduinoOTA.onError([](ota_error_t error) { ESP.restart(); });
    ArduinoOTA.begin();  /* setup the OTA server */
    // **********************************************************
    delay(1000); 
//    FastLED.addLeds<CHIPSET,DATA_PIN,COLOR_ORDER>(leds, NUM_LEDS2);   
    FastLED.addLeds<APA102, DATA_PIN, CLOCK_PIN, RGB>(leds, NUM_LEDS2);
    FastLED.setBrightness( BRIGHTNESS );
    FastLED.clear();
    turnOffAllLeds();
    //Interrupt handling******
    pinMode(14, INPUT_PULLUP);
    attachInterrupt(digitalPinToInterrupt(14), isr, FALLING);//CHANGE );
    //*************************************************************
}
void loop() {
    // Add support for OTA***************************************
    ArduinoOTA.handle();
    // 

}

void render(){
      for (int step=0; step<RESOLUTION; step++){
        for (int position=0; position<NUM_LEDS; position++){
            leds[position] = image[step][position];
        }
        FastLED.show();
//        delay(DELAY);
    }
}
void ICACHE_RAM_ATTR isr(){
//      turnOffAllLeds();
      render();
//      delay(DELAY);
}
void turnOffAllLeds(){
  for (int i=0; i<NUM_LEDS2; i++){
    // Now turn the LED off, then pause
        leds[i] = CRGB::Black;
        FastLED.show();
  }
}
